drop view Customer_view ;
drop view Diamond_view ;
drop view Non_member_view ;

create or replace view Customer_view as
SELECT 
    customer_id,
    name,
    login_id,
    type
FROM Customer where type = 'Customer' ;

create or replace TRIGGER customer_trigger
     INSTEAD OF insert ON customer_view
     FOR EACH ROW
BEGIN
    insert into Customer(
    	customer_id,
    	name,
   		login_id,
    	type)
    VALUES ( 
        :NEW.customer_id,
        :NEW.name,
        :NEW.login_id,
        'Customer') ;
END;
/

create or replace view Diamond_view as
SELECT
    customer_id,
    name,
   	login_id,
    type,
    mem_year
FROM Customer where type = 'Diamond_Member' ;

create or replace TRIGGER Diamond_Member_trigger
     INSTEAD OF insert ON Diamond_view
     FOR EACH ROW
BEGIN
    insert into Customer(
    	customer_id,
    	name,
   		login_id,
    	type,
    	mem_year)
    VALUES ( 
        :NEW.customer_id,
        :NEW.name,
        :NEW.login_id,
        'Diamond_Member',
        :NEW.mem_year) ;
END;
/

create or replace view Non_Member_view as
SELECT 
    customer_id,
    name,
   	login_id,
    type,
    credit_score
FROM Customer where type = 'Non_Member' ;

create or replace TRIGGER Non_Member_trigger
     INSTEAD OF insert ON NOn_Member_view
     FOR EACH ROW
BEGIN
    insert into Customer(
    	customer_id,
    	name,
   		login_id,
    	type,
    	credit_score)
    VALUES ( 
        :NEW.customer_id,
        :NEW.name,
        :NEW.login_id,
        'Non_Member',
        :NEW.credit_score) ;
END;
/